//
//  ContentView.swift
//  DanDoubleColumnSwiftUI
//
//  Created by Andrew Benson on 11/11/19.
//  Copyright © 2019 Nuclear Cyborg Corp. All rights reserved.
//

import SwiftUI

struct ItemPair: Identifiable {
    var id: Int

    let left: ItemView
    let right: ItemView
}

struct ContentView: View {
    var body: some View {

        let colors = [Color.red, Color.orange, Color.green, Color.blue, Color.yellow, Color.pink, Color.purple, Color.red, Color.orange, ]

        var pairs: [ItemPair] = []
        var left = ItemView(color: nil)
        var right = ItemView(color: nil)

        var nextItemIsLeftSide = true
        var index = 0
        for color in colors {
            if nextItemIsLeftSide {
                left = ItemView(color: color)
            } else {
                right = ItemView(color: color)
                let item = ItemPair(id: index, left: left, right: right)
                pairs.append(item)
            }

            nextItemIsLeftSide.toggle()
            index += 1
        }

        if !nextItemIsLeftSide {
            // make a dummy rhs
            right = ItemView(color: nil)
            let item = ItemPair(id: index, left: left, right: right)
            pairs.append(item)
        }

        return ScrollView {
            VStack {
                ForEach(pairs) { item in
                    HStack {
                        item.left
                        item.right
                    }.padding()
                }
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct ItemView: View {
    let color: Color?

    var body: some View {
        if let color = color {
            return Rectangle()
                .fill(color)
                .aspectRatio(1.0, contentMode: .fit)
                .cornerRadius(6.0)
                .shadow(color: .black, radius: 4.0, x: 2.0, y: 2.0)
        } else {
            return Rectangle()
                .fill(Color.clear)
                .aspectRatio(1.0, contentMode: .fit)
                .cornerRadius(6.0)
                .shadow(color: .black, radius: 4.0, x: 2.0, y: 2.0)
        }
    }
}
